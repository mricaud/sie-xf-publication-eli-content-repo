<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xslLib="http://www.lefebvre-sarrut.eu/ns/els/xslLib"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:fn="http://www.w3.org/2005/xpath-functions"
                xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
                xmlns:eli-article="http://www.lefebvre-sarrut.eu/ns/els/content/eli-articleELI/article"
                xmlns:xfPub="http://www.lefebvre-sarrut.eu/ns/xmlfirst/publication"
                xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                exclude-result-prefixes="#all"
                version="3.0">
  
  <xsl:import href="dependency:/eu.els.sie.xmlFirst.publication+xfPub-common/common/xsl/xfPub_editorialEntity2contentRepo.xsl"/>
  
  <xsl:param name="xfPub:ee2cr.serialize-as" select="'json'" as="xs:string"/>
  
  <xsl:variable name="xfPub:ee2cr.load-referenced-ee" select="false()" as="xs:boolean"/>
  <xsl:variable name="xfPub:ee2cr.publisher" select="'el'" as="xs:string"/>
  <xsl:variable name="xfPub:ee2cr.reorder-xjson" select="true()" as="xs:boolean"/>
  <xsl:variable name="xfPub:ee2cr.output-dir.name" select="'_DBIT/'" as="xs:string"/>
  
  <!-- OVERRIDE TEMPLATE xfPub:ee2cr.contentExtract -->
  
  <xsl:template name="xfPub:ee2cr.contentExtract">
    <xsl:param name="content" required="yes" as="element(xf:content)"/>
    <xsl:apply-templates select="$content//eli-article:figure" mode="xslLib:anyXML2xjson"/>
    <xsl:apply-templates select="$content//eli-article:a[@class = 'els-attached-file']" mode="xslLib:anyXML2xjson"/>
  </xsl:template>
  
  <!-- OVERRIDE TEMPLATE xfPub:ee2cr.getDocumentDate -->
  
  <!-- Date de publication éditoriale -> documentDate -->
  <!--<xsl:template name="xfPub:ee2cr.getDocumentDate">
    <fn:string key="documentDate">
      <xsl:value-of select="xf:getMetaValue(xf:getMeta(.,'ELS_META_datePublicationEditoriale'))!els:getIsoDateFromString(.)"/>
    </fn:string>
  </xsl:template>-->
  
  <!-- OVERRIDE TEMPLATE xfPub:ee2cr.getIsMemberOf -->
  
  <!-- isMemberOf = produit principal + produits ciblés dans la métadonnée "EFL_META_listeProduitsCommerciaux" -->
  <!-- /!\ Utilisation de la verbalisation du produit principal -->
  <!--<xsl:template name="xfPub:ee2cr.getIsMemberOf">
    <fn:array key="isMemberOf">
      <fn:string>
        <xsl:value-of select="xf:getMainProductMetaValues(.)[1]/following-sibling::verbalization[1]/normalize-space(.)"/>
      </fn:string>
    </fn:array>
  </xsl:template>-->
  
  <!-- OVERRIDE MODE xslLib:anyXML2xjson -->
  
  <!-- Titres alternatifs -> alternatives -->
  <xsl:template match="meta[@code = ('EL_META_actuTitre', 'EL_META_actuTitreSEO')]" mode="xslLib:anyXML2xjson">
    <!-- Output as 'alternatives':{'type':'xxx','title':'yyy'} -->
    <fn:array key="alternatives">
      <fn:map>
        <fn:string key="type">
          <xsl:value-of select="@code"/>
        </fn:string>
        <fn:string key="title">
          <xsl:value-of select="els:serialize(xf:getMetaValueContent(.))"/>
        </fn:string>
      </fn:map>
    </fn:array>
  </xsl:template>
  
  <!-- Chapeau SEO -> teaser -->
  <xsl:template match="meta[@code = 'EL_META_actuChapeau'][not(els:is-empty-or-whitespace(normalize-space(.)))]" mode="xslLib:anyXML2xjson">
    <fn:string key="teaser">
      <xsl:value-of select="xf:getMetaValue(.)"/>
    </fn:string>
    <!-- Pour le rappeler dans les properties -->
    <xsl:next-match/>
  </xsl:template>
  
  <!-- Auteur -> creators -->
  <xsl:template match="meta[@code = 'EL_META_actuAuteurNom']" priority="1" mode="xslLib:anyXML2xjson">
    <!-- Output as 'creators':[{'role':'xxx','agent':{'title':'yyy'}}] -->
    <fn:array key="creators">
      <fn:map>
        <fn:string key="role">
          <xsl:text>creator</xsl:text>
        </fn:string>
        <fn:map key="agent">
          <fn:string key="type">person</fn:string>
          <fn:string key="familyName">
            <xsl:value-of select="xf:getMetaValue(.)"/>
          </fn:string>
          <fn:string key="givenName">
            <xsl:value-of select="parent::metadata/meta[@code = 'EL_META_actuAuteurPrenom']/xf:getMetaValue(.)"/>
          </fn:string>
          <fn:string key="title">
            <xsl:value-of select="parent::metadata/meta[@code = 'EL_META_actuAuteurQualite']/xf:getMetaValue(.)"/>
          </fn:string>
        </fn:map>
      </fn:map>
      <xsl:if test="parent::metadata/meta[@code = 'EL_META_actuAuteurAvatar']">
        <xsl:message>[WARNING] EL_META_actuAuteurAvatar not supported yet</xsl:message>
      </xsl:if>
      <!--<xsl:if test="parent::metadata/meta[@code = 'EL_META_actuAuteurSignatureCo']">
        <fn:map>
          <fn:string key="role">
            <!-\-FIXME-\->
            <xsl:text>creator</xsl:text>
          </fn:string>
          <fn:map key="agent">
            <fn:string key="type">person</fn:string>
            <fn:string key="familyName">
              <!-\-FIXME-\->
              <xsl:value-of select="parent::metadata/meta[@code = 'EL_META_actuAuteurSignatureCo']/xf:getMetaValue(.)"/>
            </fn:string>
          </fn:map>
        </fn:map>
      </xsl:if>-->
    </fn:array>
  </xsl:template>
  <!--Les méta auteur ont déjà été traité ci-dessus-->
  <xsl:template match="meta[matches(@code,'^EL_META_actuAuteur')]" mode="xslLib:anyXML2xjson"/>

  <!-- Portrait de l'auteur -> resources -->
  <xsl:template match="meta[@code = 'EL_META_actuImagePrincipale']" priority="2" mode="xslLib:anyXML2xjson">
    <!-- Output as {'format':'xxx','source':'yyy','description':'zzz','altKeys':['www']} -->
    <xsl:variable name="resources.xjson" as="element(fn:map)">
      <fn:map>
        <!-- Ne doit pas être statique ! -->
        <fn:string key="format">
          <xsl:value-of select="(xfPub:ee2cr.getResourceFormat(.),'image/jpeg')[1]"/>
        </fn:string>
        <fn:string key="contentType">
          <xsl:text>image</xsl:text>
        </fn:string>
        <fn:string key="rights">
          <xsl:text>public</xsl:text>
        </fn:string>
        <fn:array key="altKeys">
          <fn:string>
            <xsl:value-of select="concat('flash:id:', value/ref/@xf:targetResId)"/>
          </fn:string>
          <xsl:call-template name="xfPub:ee2cr.getResourceAdditionalAltKeys">
            <xsl:with-param name="elt" select="value/ref" as="element()"/>
          </xsl:call-template>
        </fn:array>
      </fn:map>
    </xsl:variable>
    <!-- Extraction du JSON pour toutes les ressources -->
    <xsl:variable name="currentDocCustomId" select="ancestor::editorialEntity[1]/@xf:id" as="xs:string"/>
    <xsl:variable name="path" select="$currentDocCustomId || '/resources/' || value/ref/@xf:targetResId || '.resource' || $xfPub:ee2cr.serialization.ext"/>      
    <result-doc flash-item="ELS_META_actualiteAuteurPortrait"
      type="resource_json"
      doc.customId="{$currentDocCustomId}"
      href="{$xfPub:ee2cr.output-dir.path || $path}"
      href-rel="{$xfPub:ee2cr.output-dir.name || $path}"
      method="{$xfPub:ee2cr.serialization.method}"
      indent="yes"
      encoding="UTF-8">
      <xsl:sequence select="$resources.xjson"/>
    </result-doc>
    <!-- On répète la métadonnée dans le bloc properties, pour que la qualification de la ressource en tant que portrait de l'auteur puisse se faire -->
    <xsl:next-match/>
  </xsl:template>
  
  <!-- On répète la métadonnée dans le bloc properties, pour que la qualification de la ressource en tant que portrait de l'auteur puisse se faire -->
  <xsl:template match="meta[@code = 'EL_META_actuImagePrincipale']" priority="1" mode="xslLib:anyXML2xjson">
    <!-- Output as 'properties':[{'name':'xxx','value':'yyy'}] -->
    <fn:array key="properties">
      <fn:map>
        <fn:string key="name">
          <xsl:value-of select="@code"/>
        </fn:string>
        <fn:string key="value">
          <xsl:value-of select="concat('flash:id:', value/ref/@xf:targetResId)"/>
        </fn:string>
      </fn:map>
    </fn:array>
  </xsl:template>
  
  <!-- Méta PJ -> resources -->
  
  <!--On va directement chercher dans les value-->
  <xsl:template match="meta[@code = 'EL_META_actuPJ']" priority="1" mode="xslLib:anyXML2xjson">
    <!--Génération des resources associées-->
    <xsl:for-each select="value">
      <!-- Output as {'format':'xxx','source':'yyy','description':'zzz','altKeys':['www']} -->
      <xsl:variable name="resources.xjson" as="element(fn:map)">
        <fn:map>
          <fn:string key="format">
            <xsl:value-of select="(xfPub:ee2cr.getResourceFormat(.),'application/pdf')[1]"/>
          </fn:string>
          <fn:string key="contentType">
            <xsl:text>attachment</xsl:text>
          </fn:string>
          <fn:string key="rights">
            <xsl:text>public</xsl:text>
          </fn:string>
          <fn:string key="description">
            <xsl:value-of select="normalize-space(following-sibling::*[1]/self::verbalization)"/>
          </fn:string>
          <fn:array key="altKeys">
            <fn:string>
              <xsl:value-of select="concat('flash:id:', ref/@xf:targetResId)"/>
            </fn:string>
            <!--<xsl:call-template name="xfPub:ee2cr.getResourceAdditionalAltKeys">
            <xsl:with-param name="elt" select="ref" as="element()"/>
          </xsl:call-template>-->
          </fn:array>
        </fn:map>
      </xsl:variable>
      <!-- Extraction du JSON pour toutes les ressources -->
      <xsl:variable name="currentDocCustomId" select="ancestor::editorialEntity[1]/@xf:id" as="xs:string"/>
      <xsl:variable name="path" select="$currentDocCustomId || '/resources/' || ref/@xf:targetResId || '.resource' || $xfPub:ee2cr.serialization.ext"/>
      <result-doc flash-item="EL_META_actuPJ"
        type="resource_json"
        doc.customId="{$currentDocCustomId}"
        href="{$xfPub:ee2cr.output-dir.path || $path}"
        href-rel="{$xfPub:ee2cr.output-dir.name || $path}"
        method="{$xfPub:ee2cr.serialization.method}"
        indent="yes"
        encoding="UTF-8">
        <xsl:sequence select="$resources.xjson"/>
      </result-doc>
    </xsl:for-each>
    <!--Génération d'une méta globale multivaluée-->
    <!-- On répète la métadonnée dans le bloc properties, pour que la qualification de la ressource en tant que pièce jointe principale puisse se faire -->
    <!-- Output as 'properties':[{'name':'xxx','value':'yyy'}] -->
    <fn:array key="properties">
      <fn:map>
        <fn:string key="name">
          <xsl:value-of select="@code"/>
        </fn:string>
        <fn:string key="value">
          <!--FIXME !-->
          <xsl:for-each select="value">
            <xsl:value-of select="concat('flash:id:', ref/@xf:targetResId)"/>
            <xsl:if test="not(position() = last())">
              <xsl:text>;</xsl:text>
            </xsl:if>
          </xsl:for-each>
        </fn:string>
      </fn:map>
    </fn:array>
  </xsl:template>
  
  <!-- Image dans le contenu -> resources -->
  <xsl:template match="eli-article:figure" mode="xslLib:anyXML2xjson">
    <!-- Output as {'format':'xxx','source':'yyy','description':'zzz','altKeys':['www']} -->
    <xsl:variable name="resources.xjson" as="element(fn:map)">
      <fn:map>
        <!-- Ne doit pas être statique ! -->
        <fn:string key="format">
          <xsl:value-of select="(xfPub:ee2cr.getResourceFormat(.),'image/jpeg')[1]"/>
        </fn:string>
        <fn:string key="contentType">
          <xsl:text>image</xsl:text>
        </fn:string>
        <fn:string key="rights">
          <xsl:text>public</xsl:text>
        </fn:string>
        <xsl:if test="eli-article:div[@class = 'els-fig-source']">
          <fn:string key="source">
            <xsl:value-of select="normalize-space(eli-article:div[@class = 'els-fig-source'])"/>
          </fn:string>
        </xsl:if>
        <xsl:if test="eli-article:div[@class = 'els-fig-legend']">
          <fn:string key="description">
            <xsl:value-of select="normalize-space(eli-article:div[@class = 'els-fig-legend'])"/>
          </fn:string>
        </xsl:if>
        <fn:array key="altKeys">
          <fn:string>
            <xsl:value-of select="concat('flash:id:', eli-article:img/@src)"/>
          </fn:string>
          <!--<xsl:call-template name="xfPub:ee2cr.getResourceAdditionalAltKeys">
            <xsl:with-param name="elt" select="value/ref" as="element()"/>
          </xsl:call-template>-->
        </fn:array>
      </fn:map>
    </xsl:variable>
    <!-- Extraction du JSON pour toutes les ressources -->
    <xsl:variable name="currentDocCustomId" select="ancestor::editorialEntity[1]/@xf:id" as="xs:string"/>
    <xsl:variable name="path" select="$currentDocCustomId || '/resources/' || eli-article:img/@src || '.resource' || $xfPub:ee2cr.serialization.ext"/>
    <result-doc flash-item="eli-article:figure"
                type="resource_json"
                doc.customId="{$currentDocCustomId}"
                href="{$xfPub:ee2cr.output-dir.path || $path}"
                href-rel="{$xfPub:ee2cr.output-dir.name || $path}"
                method="{$xfPub:ee2cr.serialization.method}"
                indent="yes"
                encoding="UTF-8">
      <xsl:sequence select="$resources.xjson"/>
    </result-doc>
  </xsl:template>
  
  <!-- Lien fichier joint -> resources -->
  <xsl:template match="eli-article:a[@class = 'els-attached-file']" mode="xslLib:anyXML2xjson">
    <!-- Output as {'format':'xxx','source':'yyy','description':'zzz','altKeys':['www']} -->
    <xsl:variable name="resources.xjson" as="element(fn:map)">
      <fn:map>
        <!-- Ne doit pas être statique ! -->
        <fn:string key="format">
          <xsl:value-of select="(xfPub:ee2cr.getResourceFormat(.),'application/pdf')[1]"/>
        </fn:string>
        <fn:string key="contentType">
          <xsl:text>attachment</xsl:text>
        </fn:string>
        <fn:string key="rights">
          <xsl:text>public</xsl:text>
        </fn:string>
        <fn:string key="description">
          <xsl:value-of select="normalize-space(.)"/>
        </fn:string>
        <fn:array key="altKeys">
          <fn:string>
            <xsl:value-of select="concat('flash:id:', @href)"/>
          </fn:string>
          <!--<xsl:call-template name="xfPub:ee2cr.getResourceAdditionalAltKeys">
            <xsl:with-param name="elt" select="value/ref" as="element()"/>
          </xsl:call-template>-->
        </fn:array>
      </fn:map>
    </xsl:variable>
    <!-- Extraction du JSON pour toutes les ressources -->
    <xsl:variable name="currentDocCustomId" select="ancestor::editorialEntity[1]/@xf:id" as="xs:string"/>
    <xsl:variable name="path" select="$currentDocCustomId || '/resources/' || @href || '.resource' || $xfPub:ee2cr.serialization.ext"/>
    <result-doc flash-item="eli-article:a_els-attached-file"
                type="resource_json"
                doc.customId="{$currentDocCustomId}"
                href="{$xfPub:ee2cr.output-dir.path || $path}"
                href-rel="{$xfPub:ee2cr.output-dir.name || $path}"
                method="{$xfPub:ee2cr.serialization.method}"
                indent="yes"
                encoding="UTF-8">
      <xsl:sequence select="$resources.xjson"/>
    </result-doc>
  </xsl:template>
  
  <!-- Méta thèmes -> subjects -->
  <xsl:template match="meta[@code = 'EL_META_actuThemeELI']" mode="xslLib:anyXML2xjson">
    <!-- Output as 'subjects':[{'altKeys':['xxx','zzz'],'title':'yyy'}] -->
    <fn:array key="subjects">
      <xsl:for-each select="value">
        <fn:map>
          <fn:array key="altKeys">
            <fn:string>
              <xsl:value-of select="concat('EBX:',ref/@xf:targetResId,':',ref/@idRef)"/>
            </fn:string>
          </fn:array>
          <fn:string key="title">
            <xsl:value-of select="following-sibling::verbalization[1]/normalize-space(.)"/>
          </fn:string>
        </fn:map>
      </xsl:for-each>
    </fn:array>
  </xsl:template>
  
  <!--<xsl:template match="meta[@code = 'EL_META_actuCategorieActualite']" mode="xslLib:anyXML2xjson">
    <!-\- Output as 'legalAreas':[{'altKeys':['xxx','zzz'],'title':'yyy'}] -\->
    <fn:array key="properties">
      <xsl:for-each select="value">
        <fn:map>
          <fn:array key="altKeys">
            <fn:string>
              <xsl:value-of select="concat('EBX:',ref/@xf:targetResId,':',ref/@idRef)"/>
            </fn:string>
          </fn:array>
          <fn:string key="title">
            <xsl:value-of select="following-sibling::verbalization[1]/normalize-space(.)"/>
          </fn:string>
        </fn:map>
      </xsl:for-each>
    </fn:array>
  </xsl:template>-->
  
  <!-- Méta métier -> audiences -->
  <xsl:template match="meta[@code = 'EL_META_actuEtudes']" mode="xslLib:anyXML2xjson">
    <xsl:message>FIXME : ce n'est pas une audience mais plutôt une relation ?</xsl:message>
    <!-- Output as 'audiences':[{'altKeys':['xxx','zzz'],'title':'yyy'}] -->
    <fn:array key="audiences">
      <xsl:for-each select="value">
        <fn:map>
          <fn:array key="altKeys">
            <fn:string>
              <xsl:value-of select="concat('EBX:',ref/@xf:targetResId,':',ref/@idRef)"/>
            </fn:string>
          </fn:array>
          <fn:string key="title">
            <xsl:value-of select="following-sibling::verbalization[1]/normalize-space(.)"/>
          </fn:string>
        </fn:map>
      </xsl:for-each>
    </fn:array>
  </xsl:template>
  
  <!-- Métas matière -> properties -->
  <xsl:template match="meta[@code = 'EFL_META_matiere']" mode="xslLib:anyXML2xjson">
    <!-- Output as 'legalAreas':[{'altKeys':['xxx','zzz'],'title':'yyy'}] -->
    <fn:array key="legalAreas">
      <xsl:for-each select="value">
        <fn:map>
          <fn:array key="altKeys">
            <fn:string>
              <xsl:value-of select="concat('EBX:',ref/@xf:targetResId,':',ref/@idRef)"/>
            </fn:string>
          </fn:array>
          <fn:string key="title">
            <xsl:value-of select="following-sibling::verbalization[1]/normalize-space(.)"/>
          </fn:string>
        </fn:map>
      </xsl:for-each>
    </fn:array>
  </xsl:template>
  
  <!-- Méta produits commerciaux -> déjà traitée en 'isMemberOf' sur le document -->
  <xsl:template match="meta[@code = 'EFL_META_listeProduitsCommerciaux']" mode="xslLib:anyXML2xjson"/>
  
  
  <!-- OVERRIDE FUNCTION xfPub:ee2cr.getContentRepoRelationTargetType -->
  
  <!-- Les liens entre actualités pointent vers document_work -->
  <xsl:function name="xfPub:ee2cr.getContentRepoRelationTargetType" as="xs:string?">
    <xsl:param name="relation" as="element(xf:relation)"/>
    <xsl:value-of select="'document_work'"/>
  </xsl:function>
  
  
  <!-- OVERRIDE MODE xfPub:ee2cr.adaptContentNodeForContentRepo -->
  
  <!-- Hack temporaire pour cibler l'entité via a[@class = 'els-link eli-article']/@href avec le préfixe 'flash:id:' -->
  <!-- TO DO : à supprimer ultérieurement ! On passera par la relation plutôt ! -->
  <xsl:template match="eli-article:a[els:hasClass(.,'eli-article')]/@href" mode="xfPub:ee2cr.adaptContentNodeForContentRepo">
    <xsl:attribute name="{local-name()}" select="'flash:id:' || ."/>
  </xsl:template>
  
  <!-- Ajout du préfixe "flash:id:" aux attributs ciblant un média (image, fichier, etc.), pour correspondre à l'ID renseigné dans la ressource JSON stockée dans le CR -->
  <xsl:template match="eli-article:img/@src | eli-article:a[@class = 'els-attached-file']/@href" mode="xfPub:ee2cr.adaptContentNodeForContentRepo">
    <xsl:attribute name="{local-name()}" select="'flash:id:' || ."/>
  </xsl:template>
  
  <!-- FUNCTION xfPub:ee2cr.getResourceFormat -->
  
  <!-- /!\ A surcharger -> par défaut la sérialisation XML FLASH ne renseigne pas l'extension du média, seulement l'ID FLASH, à partir duquel on ne peut pas déterminer le format de la ressource -->
  <xsl:function name="xfPub:ee2cr.getResourceFormat" as="xs:string?">
    <xsl:param name="element" as="element()"/>
    <xsl:sequence select="()"/>
  </xsl:function>
  
  
  <!-- TEMPLATE xfPub:ee2cr.getResourceAdditionalAltKeys -->
  <!-- /!\ A surcharger -> permet d'insérer des entrées altKeys additionnelles à partir d'un élément appelant une ressource média, pour qu'elles soient disponibles dans le JSON de la ressource -->
  <xsl:template name="xfPub:ee2cr.getResourceAdditionalAltKeys">
    <xsl:param name="elt" required="no" select="." as="element()"/>
    <xsl:sequence select="()"/>
  </xsl:template>
  
</xsl:stylesheet>